package com.crowdar.controller;

import com.crowdar.model.Document;
import com.crowdar.repository.DocumentRepository;
import com.crowdar.services.DocumentDto;
import com.crowdar.services.DocumentServices;
import com.crowdar.services.FileManager;
import com.crowdar.services.FileManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/documents")
public class DocumentController {
    @Autowired
    DocumentRepository documentRepo;
    @Autowired
    DocumentServices docServices;
    @Autowired
    FileManager fileManager;
    @Autowired
    DocumentDto docDto;

    @GetMapping
    public List<Map<String, Object>> getDocuments() {
        return docDto.getDocumentsDto();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getDocument(@PathVariable(name = "id") Long id) {
        if (documentRepo.findById(id).isPresent()) {
            Map<String,Object>dto=docDto.makeDocumentDto(documentRepo.findById(id).get());
            return new ResponseEntity<>(dto,HttpStatus.ACCEPTED);
        } else return new ResponseEntity<>("document not found",HttpStatus.BAD_REQUEST);

    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<String> newDocument(@RequestParam String jsonDoc, @RequestParam MultipartFile file,
                                              @RequestParam String owner, @RequestParam List<String> users) {
        String filePath = "";
        Document doc;
        try {
            doc= docServices.createDocument(jsonDoc,owner,users);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.CONFLICT);
        }
        try {
            filePath = fileManager.saveFile(file);
        } catch (IOException e) {
            new ResponseEntity<>("Error uploading file", HttpStatus.CONFLICT);
        }

        doc.setResourcePath(filePath);

        documentRepo.save(doc);

        return new ResponseEntity<>("Succes!", HttpStatus.CREATED);
    }

    @GetMapping("/file/{documentId}")
    public ResponseEntity<Object> getFile(@PathVariable(name = "documentId") Long id) {
        Optional<Document> doc = documentRepo.findById(id);
        if (!doc.isPresent()) {
            return new ResponseEntity<>("file not found", HttpStatus.CONFLICT);
        }
        String path = doc.get().getResourcePath();
        Resource file;
        try {
            file = fileManager.loadFile(path);
        }catch (Exception e){
            return new ResponseEntity<>("<h1>File not found <H1>",HttpStatus.ACCEPTED);
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteDoc(@PathVariable(name = "id") Long id) {
        Optional<Document> doc = documentRepo.findById(id);
        if (!doc.isPresent()) {
            return new ResponseEntity<>("file not found", HttpStatus.CONFLICT);
        }
        String path = doc.get().getResourcePath();
        try {
            fileManager.deleteFile(path);
        } catch (Exception e) {
            System.out.println(e.getMessage());;
        }
        documentRepo.delete(doc.get());
        return new ResponseEntity<>("Delete succed", HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateDoc(@PathVariable Long id,@RequestParam String jsonDoc, @RequestParam MultipartFile file,
                                            @RequestParam String owner, @RequestParam List<String> users ){
        Optional<Document> oldDoc = documentRepo.findById(id);
        if(!oldDoc.isPresent()){
            return new ResponseEntity<>("Document not foud",HttpStatus.CONFLICT);
        }
        try {
            fileManager.deleteFile(oldDoc.get().getResourcePath());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        String filePath = "";
        Document doc;
        try {
            doc= docServices.createDocument(jsonDoc,owner,users);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.CONFLICT);
        }
        try {
            filePath = fileManager.saveFile(file);
        } catch (IOException e) {
            new ResponseEntity<>("Error uploading file", HttpStatus.CONFLICT);
        }

        doc.setResourcePath(filePath);
        doc.setId(id);
        documentRepo.save(doc);

        return new ResponseEntity<>("Succes!", HttpStatus.ACCEPTED);
    }
}
