package com.crowdar.controller;

import com.crowdar.model.Person;
import com.crowdar.repository.PersonRepository;
import com.crowdar.services.PersonDto;
import com.crowdar.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class PersonaController {
    @Autowired
    PersonRepository repo;
    @Autowired
    PersonDto userDto;
    @Autowired
    PasswordEncoder encoder;

    @GetMapping("/{id}")
    public String getUser(@PathVariable(name = "id")Long id){
        return repo.findById(id).toString();
    }

    @PostMapping("/register")
    public ResponseEntity<Object> newUser(@RequestBody Person user){
        if(!Util.validateUser(user)){
            return new ResponseEntity<>("Missing fields", HttpStatus.BAD_REQUEST);
        }
        if(repo.findByEmail(user.getEmail()).isPresent()){
         return new ResponseEntity<>("email already registered",HttpStatus.CONFLICT);
        }else {
            String encodePass=encoder.encode(user.getPassword());
            user.setPassword(encodePass);
            repo.save(user);
            return new ResponseEntity<>("Registered user successfully",HttpStatus.CREATED);
        }
    }

    @GetMapping
    public List<Map<String,Object>> getDocuments(){
        return userDto.getUsersDto();
    }
}
