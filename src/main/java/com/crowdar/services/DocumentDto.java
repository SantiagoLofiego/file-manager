package com.crowdar.services;

import com.crowdar.model.Document;
import com.crowdar.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
@Service
public class DocumentDto {
    @Autowired
    private DocumentRepository repo;

    public Map<String,Object>makeDocumentDto(Document doc){
        Map<String,Object>dto= new HashMap<>();
        dto.put("name",doc.getName());
        dto.put("id", doc.getId());
        dto.put("description", doc.getDescription());
        dto.put("owner", doc.getOwner().getEmail());
        dto.put("type", doc.getType());
        dto.put("date",doc.getDate());
        dto.put("users",doc.getUsers());
        return dto;
    }

    public List<Map<String,Object>>getDocumentsDto(){
        List<Document>docList= repo.findAll();
        List<Map<String,Object>>listDto= docList.stream().map(doc -> makeDocumentDto(doc)).collect(Collectors.toList());
        return listDto;
    }
}
