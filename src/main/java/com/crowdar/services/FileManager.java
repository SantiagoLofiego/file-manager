package com.crowdar.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileManager {
    public String saveFile (MultipartFile file) throws IOException;

    public Resource loadFile(String path);

    public void deleteFile(String path) throws IOException;
}
