package com.crowdar.services;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileManagerImpl implements FileManager {
    private final String PATH = System.getProperty("java.io.tmpdir")+"\\00temp";
    private final Path ROOT = Paths.get(PATH);

    private void init(){
        boolean created = new File(PATH).mkdir();

    }

    @Override
    public String saveFile(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        String finalPath = PATH+"\\"+fileName;
        init();
        Path path = Paths.get(finalPath);
        int counter=0;
        while(path.toFile().exists()){
            finalPath= PATH+"\\"+counter+fileName;
            path=Paths.get(finalPath);
            counter++;
        }

        file.transferTo(new File(finalPath));
        return finalPath;
    }

    @Override
    public Resource loadFile(String path) throws RuntimeException {
        try {
            Path file = Paths.get(path);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteFile(String path) throws IOException {
        Files.delete(Paths.get(path));
    }
}
