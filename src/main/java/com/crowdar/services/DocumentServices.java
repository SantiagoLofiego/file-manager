package com.crowdar.services;

import com.crowdar.model.Document;
import com.crowdar.model.Person;
import com.crowdar.repository.PersonRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentServices {
    @Autowired
    PersonRepository userRepo;

    public Document createDocument(String jsonDoc, String ownerEmail, List<String> users) throws Exception {
        Document doc = getDocument(jsonDoc);
        Person owner = getOwner(ownerEmail);
        doc.setOwner(owner);
        doc.setUsers(users);
        doc.setDate(LocalDate.now());
        boolean isValid= validateDocument(doc);
        if(!isValid){
            throw new Exception("missing fields");
        }
        return doc;
    }

    private Person getOwner(String ownerEmail) throws RuntimeException{
        Optional<Person> owner = userRepo.findByEmail(ownerEmail);
        if(!owner.isPresent()){
            throw new RuntimeException("User not found");
        }
        return owner.get();
    }

    private Document getDocument(String document) throws JsonProcessingException {
        Document doc = new Document();

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            doc = objectMapper.readValue(document,Document.class);
        } catch (JsonProcessingException e) {
            throw e;
        }

        return doc;
    }

    private boolean validateDocument(Document doc){
        if(doc.getType()==null||doc.getName().isEmpty()||doc.getOwner()==null){
            return false;
        }else return true;
    }

}
