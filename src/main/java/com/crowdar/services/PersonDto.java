package com.crowdar.services;

import com.crowdar.model.Document;
import com.crowdar.model.Person;
import com.crowdar.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
@Service
public class PersonDto {
    @Autowired
    PersonRepository repo;

    public Map<String,Object> makeDocumentDto(Person user){
        Map<String,Object>dto= new HashMap<>();
        dto.put("name",user.getNames()+" "+user.getLastNames() );
        dto.put("id", user.getId());
        dto.put("email", user.getEmail());
        return dto;
    }

    public List<Map<String,Object>> getUsersDto(){
        List<Person>userList= repo.findAll();
        List<Map<String,Object>>listDto= userList.stream().map(user -> makeDocumentDto(user)).collect(Collectors.toList());
        return listDto;
    }
}
