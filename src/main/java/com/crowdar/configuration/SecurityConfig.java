package com.crowdar.configuration;

import com.crowdar.exceptions.AuthenticationFailure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/index.html").hasAuthority("USER")
                .antMatchers("/web/login.html").permitAll()
                .antMatchers("/web/register.html").permitAll()
                .antMatchers("/user/*").permitAll()
                .antMatchers("/web/scripts/**").permitAll()
                .antMatchers("/web/styles/**").permitAll()
                .antMatchers("/web/**").hasAuthority("USER");

        http.formLogin().usernameParameter("email")
                .passwordParameter("password")
                .loginPage("/web/login.html")
                .loginProcessingUrl("/user/login")
                .failureHandler(new AuthenticationFailure());
        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.exceptionHandling().accessDeniedHandler((request, response, accessDeniedException) -> response.sendRedirect("/noAuth"));
    }

    @Autowired
    UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    /*PasswordEncoder encoder() {
        return NoOpPasswordEncoder.getInstance();
    }*/
    PasswordEncoder encoder(){return new BCryptPasswordEncoder();}


}
