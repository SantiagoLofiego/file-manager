package com.crowdar.configuration;

import com.crowdar.model.Person;
import com.crowdar.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    PersonRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Person>user = repository.findByEmail(username);
        user.orElseThrow(()-> new UsernameNotFoundException("User "+ username + " not found"));
        return new UserDetailsImpl(user.get());
    }
}
