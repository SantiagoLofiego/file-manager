package com.crowdar.model;

public enum DocumentType {
    PUBLIC,
    PRIVATE,
    DRAFT
}
