var app = new Vue({
  el: '#app',
  data: {
    data: [],
    documents: [],
    users: [],
    filterName: "",
    filterOwner: "all",
    filterDate: null
  },
  methods: {
    getData() {
      fetch("../documents")
        .then(res => {
          return res.json();
        })
        .then(data => {
          this.data = data
          app.filter();
        });
      fetch("../users")
        .then(res => {
          console.log(res);
          return res.json();
        })
        .then(data => {
          console.log(data)
          this.users = data
        });
    },
    download(id) {
      location.href = "../documents/file/" + id;
    },
    deleteFile(id) {
      if (confirm("Are you sure?")) {
        fetch("../documents/" + id, { method: 'DELETE' })
          .then(res => {
            if (res.ok) {
              location.reload();
            }
            return res.text();
          })
          .then(res => alert(res));
      }

    },
    modify(id){
      location.href ="web/document.html?id="+id;
    },
    addFile() {
      location.href = "web/document.html";
    },
    filter() {
      app.documents = [];
      let filterName = this.filterName;
      let filterOwner = this.filterOwner;
      let filterDate = this.filterDate;
      for (let doc of app.data) {
        if (doc.name.toLowerCase().includes(filterName.toLowerCase()) &&
          (doc.owner === filterOwner || filterOwner === "all") &&
          (doc.date>= filterDate||filterDate==null)) {
          this.documents.push(doc);
        }
      }
    }
  },
  created() {
    this.getData();
  }
})