
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  },
  methods: {
    ValidateEmail(email) {
      let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (email.match(mailformat)) {
        return (true)
      }
      //alert("You have entered an invalid email address!")
      return (false)
    },
    register() {
      const user = {
        "email": document.getElementById("email").value,
        "names": document.getElementById("names").value,
        "lastNames": document.getElementById("lastNames").value,
        "password": document.getElementById("pwd").value
      }
      if(!(app.ValidateEmail(user.email))){
        alert("Invalid email")
      }else{
        const options = {
          method: 'POST',
          body: JSON.stringify(user),
          headers: {
            'Content-Type': 'application/json'
          }
        }
  
        fetch("../users/register", options)
        .then(res =>{
          if(res.ok){

            location.href = "./login.html"
          }else{
            return res.text()
          }
        })
        .then(data => alert(data))
      }

      
    }
  }
})