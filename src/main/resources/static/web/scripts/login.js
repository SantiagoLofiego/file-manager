var app =new Vue({
  el: '#app',
  data: {

  },
  methods:{
    register(){
      location.href="./register.html"
    },
    login(){
      const user = {
        "email": document.getElementById("email").value,
        "password": document.getElementById("pwd").value
      }
      let formData = new FormData;
      formData.append("email", document.getElementById("email").value);
      formData.append("password", document.getElementById("pwd").value);
      console.log(formData);
      const options = {
        method: 'POST',
        body: formData
      };
      fetch("../user/login",options)
      .then(res => {
        if(res.ok){
          location.href = "../index.html"
        }
        return res.text();
      })
      .then(res => alert("Bad credentials"))
    }
  }
})