var app = new Vue({
  el: '#app',
  data: {
    users: [],
    docUsers:[],
    userEmail:"",
    doc:{},
    docId: new URLSearchParams(window.location.search).get("id"),
    title: "New Document"
  },
  methods: {
    getData() {
      fetch("../users")
      .then(res => {
        console.log(res);
        return res.json();
      })
      .then(data => {
        console.log(data)
        this.users = data
      });
    },
    getDoc(id){
      fetch("../documents/"+id)
      .then(res => {
        console.log(res);
        return res.json();
      })
      .then(data => {
        console.log(data)
        this.doc = data
        this.docUsers=data.users;
      })
      .then(() =>{
        console.log("FILL")
        this.fillFields()
        this.title="Update document";
      })
    },
    getFormData(){
      let users = this.docUsers;
      let owner = document.getElementById("owner").value;
      let file = document.getElementById('file');
      let doc = {
        "name": document.getElementById("name").value,
        "description": document.getElementById("description").value,
        "type": document.getElementById("type").value,
      };
      let formData = new FormData;
      formData.append("jsonDoc", JSON.stringify(doc));
      formData.append("file", file.files[0]);
      formData.append("owner", owner);
      formData.append("users",users);
      return formData;
    },
    upload(){
      const options = {
        method: 'POST',
        body: this.getFormData()
      };
      fetch("../documents/upload",options)
      .then(res => {
        if(res.ok){
          alert("Document created")
          location.href = "../index.html"
        }
        return res.text();
      })
      .then(res => alert(res))
    },
    update(id){
      const options = {
        method: 'PUT',
        body: this.getFormData()
      };
      fetch("../documents/"+id,options)
      .then(res => {
        if(res.ok){
          alert("Document updated")
          location.href = "../index.html"
        }
        return res.text();
      })
      .then(res => alert(res))
    },
    submit(){
      if(this.docId==null){
        this.upload();
      }else{
        this.update(this.docId);
      }
    },
    addUser(){
      let hola="hola";
      hola.length
      if(!this.docUsers.includes(this.userEmail) && this.userEmail.length>4){
        this.docUsers.push(this.userEmail);
      }
    },
    removeUser(email){
      if(this.docUsers.includes(email)){
        this.docUsers.splice(this.docUsers.indexOf(email),1);
      }
    },
    fillFields(){
      if(this.docId!=null){
        document.getElementById("name").value=this.doc.name;
        document.getElementById("type").value=this.doc.type; 
        document.getElementById("owner").value=this.doc.owner; 
        document.getElementById("description").value=this.doc.description;
      }
    }
  },
  created() {
    this.getData();
    let id=new URLSearchParams(window.location.search).get("id")
    if(id!=null){
      this.getDoc(id);
    }
  }
})